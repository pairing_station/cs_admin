class App < ApplicationRecord
    has_many :cs_method_templates
    has_many :tickets
    
    validates_presence_of :name
end
