class SynclogicWorker
  include Sneakers::Worker

  from_queue 'synclogic.cs_admin',
             WORKER_OPTIONS.merge(
                 exchange: 'synclogic',
                 exchange_type: :direct,
                 routing_key: 'synclogic.cs_admin',
                 durable: false
             )

  def work message
    message = JSON.parse( message )
    Rails.logger.warn("SyncLogic Message: #{message}")
    cs_method = CsMethod.find message["method_id"]
    results = {message: message["results"], error: message["error"]}
    cs_method.update_attributes status: "Complete", results: results
    return ack!
  end

end