class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.integer :user_id
      t.string :first_name
      t.string :last_name
      t.string :status

      t.database_authenticatable
      t.registerable
      t.recoverable
      t.rememberable
      t.trackable
      t.validatable

      t.timestamps
    end
  end
end
