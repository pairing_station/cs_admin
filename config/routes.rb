Rails.application.routes.draw do
  get '/settings' => 'settings#settings'
  
  get '/ticket' => 'tickets#ticket'
  get '/ticket_detail/:id' => 'tickets#ticket_detail'
  
  get '/tickets' => 'tickets#tickets'
  get '/apps' => 'tickets#apps_list'

  get '/cs_methods' => 'cs_methods#cs_methods_list'
  post '/cs_methods' => 'cs_methods#create_cs_method'

  get '/csmethodtemplate_list' => 'tickets#csmethodtemplate_list'
  get '/tickets_api' => 'tickets#tickets_list'
  post '/tickets_api' => 'tickets#create_ticket'

  get '/users' => 'tickets#users_list'

  post '/create_new_ticket' => 'new_ticket#create_ticket'
  get '/new_ticket' => 'new_ticket#fill_new_ticket'
  get '/created_ticket' => 'tickets#get_new_ticket'

  get '/all_tickets' => 'new_ticket#all_tickets'
  post '/all_tickets' => 'new_ticket#create_ticket'
 
  root 'tickets#tickets'
end
