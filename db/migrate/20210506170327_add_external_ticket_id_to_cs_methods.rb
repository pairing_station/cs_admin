class AddExternalTicketIdToCsMethods < ActiveRecord::Migration[6.0]
  def change
    add_column :cs_methods, :external_ticket_id, :integer
  end
end
