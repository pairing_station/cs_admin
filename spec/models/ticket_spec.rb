require 'rails_helper'

RSpec.describe Ticket, :type => :model do

    subject { 
            described_class.new(
                external_ticket_id: 9999,
                app: FactoryBot.create(:app),
                shop_url: "mimi@shopify",
                user: FactoryBot.create(:user),
                status: "Blocked"
            )  
     }

    describe "validations" do

      it "has a valid factory" do
        expect(FactoryBot.create(:ticket)).to be_valid
      end

      it "is valid with valid attributes" do
        expect(subject).to be_valid
      end

      it "is not valid without a external_ticket_id" do
        subject.external_ticket_id = nil
        expect(subject).to_not be_valid
      end

      it "is not valid without a shop_url" do
        subject.shop_url = nil
        expect(subject).to_not be_valid
      end

      it "is not valid without a app_id" do
        subject.app_id = nil
        expect(subject).to_not be_valid
      end

      it "is not valid without a user_id" do
        subject.user_id = nil
        expect(subject).to_not be_valid
      end

      it "is not valid without a status" do
        subject.status = nil
        expect(subject).to_not be_valid
      end

    end

    describe "Associations" do

      it { should belong_to(:app) }
      it { should belong_to(:user) }

    end
end
