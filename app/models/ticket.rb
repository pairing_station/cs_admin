class Ticket < ApplicationRecord
    belongs_to :user
    belongs_to :app

    validates_presence_of :user_id, :external_ticket_id, :shop_url, :app_id, :status

end
