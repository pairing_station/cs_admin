import React, { Component } from "react";
import PropTypes from "prop-types";
import Dropdown from "react-bootstrap/Dropdown";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import Jumbotron from "react-bootstrap/Jumbotron";
import "../../assets/stylesheets/application.css";
import {
  getAppsList,
  getCsMethodsList,
  getTicketApi,
  getCsMethodTemplateList,
  getUsersList,
  getNewTicket
} from "./service/allServices";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { BrowserRouter as Router } from "react-router-dom";
import Alert from "react-bootstrap/Alert";
import { useHistory } from "react-router-dom";
import { withRouter } from "react-router-dom";
import queryString from "query-string";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import axios from 'axios';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import CreationModal from './newCsMethodModal';
import ListGroup from 'react-bootstrap/ListGroup'

class Ticket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apps: [],
      cs_methods: [],
      ticket: [],
      csMethodTemplates: [],
      showPage: true,
      id: "",
      help: false,
      syncLogicCsMethodTemplates: [],
      orderLogicCsMethodTemplates: [],
      emailAlchemyCsMethodTemplates: [],
      selectedCsMethodTemplateId: null,
      selectedCsMethodTemplate: {id: 0, arguments: {Field0: "bla", Field1: "bool"}},
      argument: {},
      users: [],
      userFirstName: "",
      userLastName: "",
      createdAt: null,
      editedDate: null,
      results: {result: "yea", mof: "tara"},
      external_ticket_id: null,
      shop_url: "",
      field0: "",
      field1: "",
      app_id: null,
      showModal: false,
      current_app_name: "",
      new_ticket: [],
      show_fields: false,
      result_content: []
    };
    this.makeStatusClosed = this.makeStatusClosed.bind(this);
    this.makeStatusActive = this.makeStatusActive.bind(this);
    this.makeStatusPending = this.makeStatusPending.bind(this);
    this.makeStatusBlocked = this.makeStatusBlocked.bind(this);
    this.showHelp = this.showHelp.bind(this);
    this.getSyncLogicCsMethodTemplates = this.getSyncLogicCsMethodTemplates.bind(this);
    this.getOrderLogicCsMethodTemplates = this.getOrderLogicCsMethodTemplates.bind(this);
    this.getEmailAlchemyCsMethodTemplates = this.getEmailAlchemyCsMethodTemplates.bind(this);
    this.handlingTemplates = this.handlingTemplates.bind(this);
    this.onCsMethodTemplateClick = this.onCsMethodTemplateClick.bind(this);
    this.getUserName = this.getUserName.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onHandleChange = this.onHandleChange.bind(this);
    this.splitDate = this.splitDate.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
    this.assignInputValuesToSelectedCsMethodTemplate = this.assignInputValuesToSelectedCsMethodTemplate.bind(this);
    this.setDefaultAppName = this.setDefaultAppName.bind(this);
    this.updateTicket = this.updateTicket.bind(this);
    this.refreshPage = this.refreshPage.bind(this);
    this.showResult = this.showResult.bind(this);
  }

  componentDidMount() {
    this.fetchAppsList();
    this.fetchCsMethodsList();
    this.fetchTicketApi();
    this.fetchCsMethodTemplateList();
    this.fetchUsersList();
    this.fetchNewTicket();
  }

  async fetchAppsList() {
    const { data } = await getAppsList();
    const { list, status } = data;
    this.setState({ apps: list });
  }

  async fetchCsMethodsList() {
    const { data } = await getCsMethodsList();
    const { list, status } = data;
    if (typeof list != "undefined") {
      console.log(Object.keys(list[0].arguments));
    }
    this.setState({ cs_methods: list });
  }

  async fetchTicketApi() {
    const id = this.parseId();
    const { data } = await getTicketApi(id);
    const { ticket, status } = data;
    this.setState({ ticket: ticket });
  }

  async fetchNewTicket(){
    const { data } = await getNewTicket();
    const { new_ticket, status } = data;
    this.setState({ new_ticket: new_ticket });
  }

  async fetchCsMethodTemplateList() {
    const { data } = await getCsMethodTemplateList();
    const { list, status } = data;
    this.setState({ csMethodTemplates: list });
  }

  async fetchUsersList() {
    const { data } = await getUsersList();
    const { list, status } = data;
    this.setState({ users: list });
  }

  handlingTemplates(){
    if(this.state.current_app_name == "SyncLogic"){
      this.getSyncLogicCsMethodTemplates();
    } else if(this.state.current_app_name == "OrderLogic"){
      this.getOrderLogicCsMethodTemplates();
    } else if(this.state.current_app_name == "EmailAlchemy"){
      this.getEmailAlchemyCsMethodTemplates();
    } else {console.log("Unknown app");}
  }

  async getSyncLogicCsMethodTemplates() {
    const allCsMethodTemplatesForSl = this.state.csMethodTemplates;
    let syncLogic = this.state.syncLogicCsMethodTemplates;
    syncLogic = [];
    let ol = this.state.orderLogicCsMethodTemplates;
    let ea = this.state.emailAlchemyCsMethodTemplates;

    for (let i = 0; i < allCsMethodTemplatesForSl.length; i++) {
      if (allCsMethodTemplatesForSl[i]["app_id"] === 1)
        if (syncLogic.includes(allCsMethodTemplatesForSl[i]) === false){
        ol = [];
        ol.save;
        ea = [];
        ea.save;
        syncLogic.push(allCsMethodTemplatesForSl[i]);
        syncLogic.save;

        await this.setState({
          app_id: 1,
          syncLogicCsMethodTemplates: syncLogic,
          orderLogicCsMethodTemplates: ol,
          emailAlchemyCsMethodTemplates: ea,
        });
      }
    }
  }

  async getOrderLogicCsMethodTemplates() {
    const allCsMethodTemplatesForOl = this.state.csMethodTemplates;
    let orderLogic = this.state.orderLogicCsMethodTemplates;
    orderLogic = [];
    let sl = this.state.syncLogicCsMethodTemplates;
    let ea = this.state.emailAlchemyCsMethodTemplates;

    for (let i = 0; i < allCsMethodTemplatesForOl.length; i++) {
      if (allCsMethodTemplatesForOl[i]["app_id"] === 2) {
        if (orderLogic.includes(allCsMethodTemplatesForOl[i]) === false)
        sl = [];
        sl.save;
        ea = [];
        ea.save;
        orderLogic.push(allCsMethodTemplatesForOl[i]);
        orderLogic.save;

       await this.setState({
          app_id: 2,
          orderLogicCsMethodTemplates: orderLogic,
          syncLogicCsMethodTemplates: sl,
          emailAlchemyCsMethodTemplates: ea,
        });
      }
    }
  }

  async getEmailAlchemyCsMethodTemplates() {
    const allCsMethodTemplatesForEa = this.state.csMethodTemplates;
    let emailAlchemy = this.state.emailAlchemyCsMethodTemplates;
    emailAlchemy = [];
    let sl = this.state.syncLogicCsMethodTemplates;
    let ol = this.state.orderLogicCsMethodTemplates;

    for (let i = 0; i < allCsMethodTemplatesForEa.length; i++) {
      if (allCsMethodTemplatesForEa[i]["app_id"] === 3) {
        if (emailAlchemy.includes(allCsMethodTemplatesForEa[i]) === false)
        ol = [];
        ol.save;
        sl = [];
        sl.save;
        emailAlchemy.push(allCsMethodTemplatesForEa[i]);
        emailAlchemy.save;

        await this.setState({
          app_id: 3,
          emailAlchemyCsMethodTemplates: emailAlchemy,
          syncLogicCsMethodTemplates: sl,
          orderLogicCsMethodTemplates: ol,
        });
      }
    }
  }

  async onCsMethodTemplateClick(id){
    let myId = id;
    this.setState({ show_fields: true })
     await this.setState({
          selectedCsMethodTemplateId: myId
        })
    let allTemplates = this.state.csMethodTemplates;
    let selectedTemplate = this.state.selectedCsMethodTemplate;
    selectedTemplate = {};

    for(let i=0; i < allTemplates.length; i++){
       if(allTemplates[i]["id"] === this.state.selectedCsMethodTemplateId){
          selectedTemplate = allTemplates[i];
          selectedTemplate.save;

          this.setState(
            ({ selectedCsMethodTemplate }) => ({
              selectedCsMethodTemplate: selectedTemplate
            }), () => {
              this.setState(({ selectedCsMethodTemplate }) => ({
                selectedCsMethodTemplate: selectedTemplate
              }))
            } )
       }
    }
  }
  
  parseId() {
    const url = this.props.location.search;
    const result = queryString.parse(url);
    const { id } = result;
    this.setState({ id: id });
    return id;
  }

  makeStatusClosed() {
    const newTicket = this.state.ticket;
    newTicket.status = "Closed";
    this.setState({
      ticket: newTicket,
    });
  }

  makeStatusActive() {
    const newTicket = this.state.ticket;
    newTicket.status = "Active";
    this.setState({
      ticket: newTicket,
    });
  }

  makeStatusPending() {
    const newTicket = this.state.ticket;
    newTicket.status = "Pending";
    this.setState({
      ticket: newTicket,
    });
  }

  makeStatusBlocked() {
    const newTicket = this.state.ticket;
    newTicket.status = "Blocked";
    this.setState({
      ticket: newTicket,
    });
  }

  showHelp() {
    this.setState({
      help: true,
    });
  }
  
  onHandleChange = (evt) => {
    const value = evt.target.value;
    this.setState({
    ...this.state,
    [evt.target.name]: value
  });
    console.log("extID: ", this.state.external_ticket_id);
    console.log("shop_url: ", this.state.shop_url);
    console.log("field0: ", this.state.field0);
    console.log("field1: ", this.state.field1);
  }

  onSubmit = () => {
    console.log(this.state.selectedCsMethodTemplate["arguments"]);
      axios({
        method: 'post',
        url: 'http://localhost/cs_methods',
        withCredentials: true,
        data: {
          ticket_id: this.state.ticket.id,
          arguments: this.state.argument,
          results: this.state.results,
          status: this.state.ticket.status,
          cs_method_template_id: this.state.selectedCsMethodTemplateId,
          user_id: this.state.ticket["user_id"],
          external_ticket_id: this.state.external_ticket_id || this.state.ticket.external_ticket_id,
          app_id: this.state.app_id || this.state.ticket.app_id
      },
      headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' }
    })
    .then(response => {
      console.log(response.data)
    })
    .catch(error => {
      console.log(error)
      if (error.response) {
          console.log("--------------------------------------------------")
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
      } else if (error.request) {
          console.log("*************************")
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
      } else {
          console.log("++++++++++++++++++++++++")
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
      }
      console.log(error.config);
  })

    console.log("ticket id: ", this.state.ticket.id);
    console.log("user id: ", this.state.ticket["user_id"]);
    console.log("status: ", this.state.ticket.status);
    console.log("cs_method_template_id: ", this.state.selectedCsMethodTemplateId);
    console.log("arguments: ", this.state.argument);
    console.log("results: ", this.state.results);
    console.log("external ticket id: ", this.state.external_ticket_id || this.state.ticket.external_ticket_id);
    console.log("app_id: ", this.state.app_id);
    }  

  updateTicket = () => {
    if(this.state.external_ticket_id != null){
      axios({
        method: 'post',
        url: 'http://localhost/all_tickets',
        data: {
          external_ticket_id: this.state.external_ticket_id || this.state.ticket.external_ticket_id,
          shop_url: this.state.shop_url || this.state.ticket.shop_url,
          status: this.state.ticket.status,
          user_id: this.state.ticket["user_id"],
          app_id: this.state.app_id || this.state.ticket.app_id  
      },
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        console.log(response)
      })
      .catch((error) => {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
    })}  
    console.log("user id: ", this.state.ticket["user_id"]);
    console.log("status: ", this.state.ticket.status);
    console.log("shop url: ", this.state.shop_url);
    console.log("external ticket id: ", this.state.external_ticket_id || this.state.ticket.external_ticket_id);
    console.log("app_id: ", this.state.app_id);
    }

  refreshPage = () => {
      window.location.reload();
   }

  async splitDate(){
    let date = this.state.ticket.created_at
    let newDate = date.slice(0, 10);
    await this.setState({
      editedDate: newDate
    })
  }

  closeModal(){
    this.setState({
      showModal: false
    })
  }

  openModal(){
    this.setState({
      showModal: true
    })
  }

  assignInputValuesToSelectedCsMethodTemplate(){
    let newObject = {};
    let first = Object.keys(this.state.selectedCsMethodTemplate["arguments"])[0];
    let val0 = this.state.field0;
    let second = Object.keys(this.state.selectedCsMethodTemplate["arguments"])[1];
    let val1 = this.state.field1;
    newObject[`${first}`] = val0;
    newObject[`${second}`] = val1;

        this.setState(
          ({ argument }) => ({
            argument: newObject
          }), () => {
            this.setState(({ argument }) => ({
              argument: newObject
            }))
          } )
  }

 async setDefaultAppName(){
    let current_tic = this.state.ticket
    if(current_tic["app_id"] == 1){
     await this.setState({
        current_app_name: "SyncLogic"
      })
    } else if(current_tic["app_id"] == 2){
    await this.setState({
        current_app_name: "OrderLogic"
      })
    } else {
    await this.setState({
        current_app_name: "EmailAlchemy"
      })
    }
  }

  async getUserName(){
    let users = this.state.users;
    let current_user_id = this.state.ticket.user_id;
    for(let i=0; i < users.length; i++){
      if(users[i]["id"] == current_user_id){
        await this.setState({
          userFirstName: users[i]["first_name"],
          userLastName: users[i]["last_name"]
        })
      }
    }
  }

  async showResult(){
    let template = "Command - " + this.state.selectedCsMethodTemplate.name;
    let arg = "CsMethods - " + Object.entries(this.state.argument);
    let result = "Result - " + Object.entries(this.state.results);
    let result_cont = [];
    result_cont.push(template, arg, result);
    await this.setState({
      result_content: result_cont
    })
    console.log("result ", this.state.result_content);
  }

  render() {
    const { history } = this.props;

    const hideFields = { display: "none" };
    const showFields = { display: "" };
    
    let i = 0;
    
    const {
      syncLogicCsMethodTemplates,
      orderLogicCsMethodTemplates,
      emailAlchemyCsMethodTemplates,
      selectedCsMethodTemplate,
      show_fields,
      current_app_name
    } = this.state;

    console.log("templ name", selectedCsMethodTemplate.name);
 
    if (current_app_name == "SyncLogic") {
      syncLogicCsMethodTemplates.map((syncLogicCsMethodTemplate) => (
        <OverlayTrigger
          placement="bottom"
          overlay={
            <Tooltip id="button-tooltip-2">
              {syncLogicCsMethodTemplate.help}
            </Tooltip>
          }
          key={syncLogicCsMethodTemplate.id}
        >
          <Dropdown.Item
            key={syncLogicCsMethodTemplate.id}
            onClick={() =>
              this.onCsMethodTemplateClick(
                syncLogicCsMethodTemplate.id
              )
            }
          >
            {syncLogicCsMethodTemplate.name}
          </Dropdown.Item>
        </OverlayTrigger>
      ))
    } else if (current_app_name == "OrderLogic") {
      orderLogicCsMethodTemplates.map((orderLogicCsMethodTemplate) => (
        <OverlayTrigger
          placement="bottom"
          overlay={
            <Tooltip id="button-tooltip-2">
              {orderLogicCsMethodTemplate.help}
            </Tooltip>
          }
          key={orderLogicCsMethodTemplate.id}
        >
          <Dropdown.Item
            key={orderLogicCsMethodTemplate.id}
            onClick={() =>
              this.onCsMethodTemplateClick(
                orderLogicCsMethodTemplate.id
              )
            }
          >
            {orderLogicCsMethodTemplate.name}
          </Dropdown.Item>
        </OverlayTrigger>
      ));
    } else if (current_app_name == "EmailAlchemy") {
      emailAlchemyCsMethodTemplates.map((emailAlchemyCsMethodTemplate) => (
        <OverlayTrigger
          placement="bottom"
          overlay={
            <Tooltip id="button-tooltip-2">
              {emailAlchemyCsMethodTemplate.help}
            </Tooltip>
          }
          key={emailAlchemyCsMethodTemplate.id}
        >
          <Dropdown.Item
            key={emailAlchemyCsMethodTemplate.id}
            onClick={() =>
              this.onCsMethodTemplateClick(
                emailAlchemyCsMethodTemplate.id
              )
            }
          >
            {emailAlchemyCsMethodTemplate.name}
          </Dropdown.Item>
        </OverlayTrigger>
      ));
    } else {
      null;
    }

    return (
      <Router>
        <Alert dismissible onClose={() => history.push(`/tickets`)}>
          <div>
            <div>
              <Row style={{ width: "100%" }}>
                <Col sm={1}>
                  <Navbar
                    expand="sm"
                    style={{ backgroundColor: "#13547a", height: "950px", width: "70px" }}
                    className="flex-column"
                  >

                    <Nav className="flex-column">

                      <Nav.Link style={{ color: "white" }}
                        onClick={() => history.push(`/tickets`)}>
                      <span style={{ fontSize: "25px" }}>
                      <i className="fas fa-th-list"></i>
                      </span>
                      </Nav.Link>

                      <Nav.Link style={{ color: "white" }}
                      onClick={() => history.push(`/settings`)}>
                      <span style={{ fontSize: "25px" }}>
                       <i className="fas fa-cogs"></i>
                      </span>
                      </Nav.Link>

                    </Nav>
                  </Navbar>
                </Col>

                <Col sm={11} style={{ width: "100%", margin: "0" }}>
                  <Form
                    style={{
                      backgroundColor: "rgb(70,136,155)",
                      paddingTop: "35px",
                      paddingRight: "20px",
                      paddingLeft: "20px",
                      paddingBottom: "1px",
                    }}
                    onMouseEnter={() => {this.setDefaultAppName(); this.getUserName();}}
                  >
                    <Form.Group as={Row} onMouseEnter={() => {this.handlingTemplates(); this.splitDate();}}>
                      <Form.Label column sm={1} style={{ color: "white" }}>
                        External Ticket Id
                      </Form.Label>
                      <Col sm={3}>
                        <Form.Control type="number" name="external_ticket_id" value={this.state.external_ticket_id || this.state.ticket.external_ticket_id} 
                        onChange={this.onHandleChange} />
                      </Col>
                      <Col sm={2}></Col>
                      <Form.Label column sm={1} style={{ color: "white" }}>
                        Date
                      </Form.Label>
                      <Col sm={4}>
                        <Form.Control
                          plaintext
                          style={{ color: "white" }}
                          readOnly
                          defaultValue={this.state.editedDate}
                        />
                      </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                      <Form.Label column sm={1} style={{ color: "white" }}>
                        Shop
                      </Form.Label>
                      <Col sm={3}>
                        <Form.Control
                          type="text"
                          name="shop_url"
                          style={{ color: "#495057", backgroundColor: "white" }}
                          value={this.state.shop_url || this.state.ticket.shop_url}
                          onChange={this.onHandleChange}
                        />
                      </Col>
                      <Col sm={2}></Col>

                      <Form.Label column sm={1} style={{ color: "white" }}>
                        App
                      </Form.Label>
                      <Col sm={2}>
                        <Form.Control
                          plaintext
                          style={{ color: "white" }}
                          readOnly
                          defaultValue={this.state.current_app_name}
                        />
                      </Col>
                    </Form.Group>

                    <Form.Group as={Row} style={{marginTop: "35px"}}>

                    <Form.Label column sm={1} style={{ color: "white" }}>
                        User
                      </Form.Label>
                      <Col sm={3}>
                        <Form.Control
                          plaintext
                          style={{ color: "white" }}
                          readOnly
                          defaultValue={this.state.userFirstName + this.state.userLastName}
                        />
                      </Col>
                      <Col sm={2}></Col>

                      <Col sm={1}>
                      <Form.Label style={{ color: "white" }}>
                        Status
                      </Form.Label>
                      </Col>

                      <Col sm={2}>
                      <Dropdown>
                        <Dropdown.Toggle className="button-group" id="dropdown-basic">
                          {this.state.ticket.status}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                          <Dropdown.Item onClick={this.makeStatusActive}>Active</Dropdown.Item>
                          <Dropdown.Item onClick={this.makeStatusPending}>Pending</Dropdown.Item>
                          <Dropdown.Item onClick={this.makeStatusClosed}>Closed</Dropdown.Item>
                          <Dropdown.Item onClick={this.makeStatusBlocked}>Blocked</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                      </Col>
                      
                    
                    </Form.Group>

                    <Form.Group as={Row} style={{ marginTop: "35px" }}>
                      <Col sm={10}></Col>

                      <Col sm={2}>
                        <Button
                          style={{
                            backgroundColor: "#13547a",
                            borderColor: "#13547a",
                          }}
                          onClick={() => { this.updateTicket(); this.refreshPage(); }}
                        >
                          Update Ticket
                        </Button>
                      </Col>

                    </Form.Group>
                  </Form>
                  <Form style={{ margin: "20px 20px 20px 20px" }}>
                    <Form.Row>
                      <Jumbotron
                        style={{
                          width: "1600px",
                          height: "300px",
                          background: "rgb(70,136,155)",
                          color: "white",
                        }}
                      >
                        {this.state.result_content.map((result) => 
                        (<ListGroup.Item style={{background: "rgb(70, 136, 155)", color:"white"}}>{result}</ListGroup.Item>))}
                      </Jumbotron>
                      <Form.Row style={{ marginBottom: "30px", marginLeft: "5px" }}>
                        <Button
                          style={{
                            backgroundColor: "#13547a",
                            borderColor: "#13547a",
                          }}
                        >
                          Refresh
                        </Button>
                        </Form.Row>
                    </Form.Row>

                    <Form.Group as={Row} style={{ paddingBottom: "15px" }}>
                      <Col sm={3}>
                        <Dropdown onClick={this.showTemplates}>
                          <Dropdown.Toggle
                            style={{
                              backgroundColor: "#13547a",
                              borderColor: "#13547a",
                            }}
                            id="dropdown-basic"
                          >
                            Select a Command
                          </Dropdown.Toggle>
                          <Dropdown.Menu>
                            {syncLogicCsMethodTemplates.map(
                              (syncLogicCsMethodTemplate) => (
                                <OverlayTrigger
                                  placement="bottom"
                                  overlay={
                                    <Tooltip id="button-tooltip-2">
                                      {syncLogicCsMethodTemplate.help}
                                    </Tooltip>
                                  }
                                  key={syncLogicCsMethodTemplate.id}
                                >
                                  <Dropdown.Item
                                    key={syncLogicCsMethodTemplate.id}
                                    onClick={() =>
                                      this.onCsMethodTemplateClick(syncLogicCsMethodTemplate.id)}
                                  >
                                    {syncLogicCsMethodTemplate.name}
                                  </Dropdown.Item>
                                </OverlayTrigger>
                              )
                            )}

                            {orderLogicCsMethodTemplates.map(
                              (orderLogicCsMethodTemplate) => (
                                <OverlayTrigger
                                  placement="bottom"
                                  overlay={
                                    <Tooltip id="button-tooltip-2">
                                      {orderLogicCsMethodTemplate.help}
                                    </Tooltip>
                                  }
                                  key={orderLogicCsMethodTemplate.id}
                                >
                                  <Dropdown.Item
                                    key={orderLogicCsMethodTemplate.id}
                                    onClick={() =>
                                      this.onCsMethodTemplateClick(orderLogicCsMethodTemplate.id)
                                    }
                                  >
                                    {orderLogicCsMethodTemplate.name}
                                  </Dropdown.Item>
                                </OverlayTrigger>
                              )
                            )}

                            {emailAlchemyCsMethodTemplates.map(
                              (emailAlchemyCsMethodTemplate) => (
                                <OverlayTrigger
                                  placement="bottom"
                                  overlay={
                                    <Tooltip id="button-tooltip-2">
                                      {emailAlchemyCsMethodTemplate.help}
                                    </Tooltip>
                                  }
                                  key={emailAlchemyCsMethodTemplate.id}
                                >
                                  <Dropdown.Item
                                    key={emailAlchemyCsMethodTemplate.id}
                                    onClick={() =>
                                      this.onCsMethodTemplateClick(emailAlchemyCsMethodTemplate.id)
                                    }
                                  >
                                    {emailAlchemyCsMethodTemplate.name}
                                  </Dropdown.Item>
                                </OverlayTrigger>
                              )
                            )}
                          </Dropdown.Menu>
                        </Dropdown>
                      </Col>

                    
                    <Col sm={6} style={{ marginLeft: "20px" }} style={
                                  show_fields == true
                                    ? showFields
                                    : hideFields
                                }>
                    <h6 style={{ color: "white" }}>
                      Fill in these fields to complete command:
                    </h6>
                   
                    {Object.keys(selectedCsMethodTemplate["arguments"]).map((key, i) => (
                        <Form.Group as={Row} style={{ paddingTop: "20px", marginLeft: "10px" }}  key={i}>
                          <Form.Label style={{ width: '80px', color: "white" }}>
                            {key}
                          </Form.Label>
                          <Col style={{ width: '180px' }}>
                            <Form.Control placeholder="Enter value" type="text" name={"field" + i}
                            value={this.state['field'+i]} onChange={this.onHandleChange} /> <div style={{display: "none"}}>{i++}</div>
                          </Col>
                        </Form.Group>
                    ))}

                    <ButtonToolbar style={{ marginTop: "30px" }} onMouseEnter={this.assignInputValuesToSelectedCsMethodTemplate}>
                      <Button style={{
                            backgroundColor: "#13547a",
                            borderColor: "#13547a" }}>
                      <a
                        type="submit"
                        data-backdrop="static" data-keyboard="true"
                        style={{ color: "white", margin: "auto" }}
                        onClick={() => { this.assignInputValuesToSelectedCsMethodTemplate(); this.onSubmit(); this.showResult(); }}
                      >
                        Run Command
                      </a>
                      </Button>

                      <CreationModal show={this.state.showModal}
                      onHide={this.closeModal} />
                      
                      </ButtonToolbar>
                    </Col>

                    </Form.Group>
                  </Form>
                </Col>
              </Row>
            </div>
          </div>
        </Alert>
      </Router>
    );
  }
}
export default withRouter(Ticket);
