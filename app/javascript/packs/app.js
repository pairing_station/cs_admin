import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Tickets from "./tickets";
import Ticket from "./ticket";
import Settings from "./settings";
import NewTicket from "./newTicket";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={Tickets} />
          <Route path="/tickets" exact component={Tickets} />
          <Route path="/ticket" exact component={Ticket} />
          <Route path="/settings" exact component={Settings} />
          <Route path="/new_ticket" exact component={NewTicket} />
        </Switch>
      </Router>
    );
  }
}

export default App;
