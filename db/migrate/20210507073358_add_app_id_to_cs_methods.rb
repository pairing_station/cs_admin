class AddAppIdToCsMethods < ActiveRecord::Migration[6.0]
  def change
    add_column :cs_methods, :app_id, :integer
  end
end
