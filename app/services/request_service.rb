class RequestService < ApplicationService
  class << self
    def get_connection
      @@connection ||= begin
        bunny = Bunny.new (ENV['AMQP_URL'])
        bunny.start
        bunny
      end
    end

    def get_instance
      RequestService.new channel: get_connection.create_channel
    end

    def send_request app_name:, method_name:, method_args:, method_id:
      request_service = get_instance
      request_service.send_request app_name: app_name, method_name: method_name, method_args: method_args, method_id: method_id
      request_service.close_channel!
    end
  end

  def initialize channel:
    @channel = channel
    @exchange = @channel.direct("cs_admin")
  end

  def close_channel!
    @channel.close
  end

  def send_request app_name:, method_name:, method_args:, method_id:
    message = {
        method_name: method_name,
        method_args: method_args,
        method_id: method_id
    }.to_json

    @exchange.publish message, routing_key: "cs_admin.#{app_name}"
  end
end