arguments_hash = {"shop"=> "name or int"}

FactoryBot.define do
  factory :cs_method_template do
    association :app, :factory => :app
    arguments {arguments_hash}
    help {"help"}
    name {"products_count"}
  end
end