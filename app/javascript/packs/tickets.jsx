import React, { Component } from "react";
import PropTypes from "prop-types";
import Table from "react-bootstrap/Table";
import Dropdown from "react-bootstrap/Dropdown";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import { getTicketsList } from "./service/allServices";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { BrowserRouter as Router } from "react-router-dom";
import { withRouter } from "react-router-dom";
import "../../assets/stylesheets/application.css";
import _ from "lodash";

class Tickets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tickets_list: [],
      isDateVisible: false,
      isExternalTicketIdVisible: false,
      isShopVisible: false,
      isAppVisible: false,
      isStatusVisible: false,
      isMethodsVisible: false,
      isUserVisible: false,
      currentSort: "default",
      isDateChecked: false,
      isExternalTicketIdChecked: false,
      isShopChecked: false,
      isAppChecked: false,
      isStatusChecked: false,
      isMethodsChecked: false,
      isUserChecked: false,
      currentPage: 1,
      ticketsPerPage: 3,
      paginatedTickets: [],
      externalTicketId: 0,
      searchResult: [],
      shopUrl: "",
      urlSearchResult: [],
      showSearchResultColumn: false,
      showUrlSearchResultColumn: false
    };
    this.makeDateVisible = this.makeDateVisible.bind(this);
    this.makeExternalTicketIdVisible = this.makeExternalTicketIdVisible.bind(this);
    this.makeShopVisible = this.makeShopVisible.bind(this);
    this.makeAppVisible = this.makeAppVisible.bind(this);
    this.makeStatusVisible = this.makeStatusVisible.bind(this);
    this.makeMethodsVisible = this.makeMethodsVisible.bind(this);
    this.makeUserVisible = this.makeUserVisible.bind(this);
    this.onSortChange = this.onSortChange.bind(this);
    this.isDateCheckmarked = this.isDateCheckmarked.bind(this);
    this.handlePaginationClick = this.handlePaginationClick.bind(this);
    this.setPaginatedTicketsList = this.setPaginatedTicketsList.bind(this);
    this.onHandleChange = this.onHandleChange.bind(this);
    this.onHandleUrlChange = this.onHandleUrlChange.bind(this);
    this.searchTicket = this.searchTicket.bind(this);
    this.searchTicketUsingUrl = this.searchTicketUsingUrl.bind(this);
  }

  componentDidMount() {
    this.fetchTicketsList();
  }

  async fetchTicketsList() {
    const { data } = await getTicketsList();
    const { list, status } = data;
    this.setState({ tickets_list: list });
  }

  makeDateVisible() {
    this.setState({
      isDateVisible: !this.state.isDateVisible,
    });
  }
  makeExternalTicketIdVisible() {
    this.setState({
      isExternalTicketIdVisible: !this.state.isExternalTicketIdVisible,
    });
  }
  makeShopVisible() {
    this.setState({
      isShopVisible: !this.state.isShopVisible,
    });
  }
  makeAppVisible() {
    this.setState({
      isAppVisible: !this.state.isAppVisible,
    });
  }
  makeStatusVisible() {
    this.setState({
      isStatusVisible: !this.state.isStatusVisible,
    });
  }
  makeMethodsVisible() {
    this.setState({
      isMethodsVisible: !this.state.isMethodsVisible,
    });
  }
  makeUserVisible() {
    this.setState({
      isUserVisible: !this.state.isUserVisible,
    });
  }

  onSortChange() {
    const { currentSort } = this.state;
    let nextSort;
    if (currentSort === "down") nextSort = "up";
    else if (currentSort === "up") nextSort = "default";
    else if (currentSort === "default") nextSort = "down";

    this.setState({
      currentSort: nextSort,
    });
    console.log(nextSort);
  }

  isDateCheckmarked() {
    this.setState({
      isDateChecked: !this.state.isDateChecked,
    });
  }

  isExternalTicketIdCheckmarked() {
    this.setState({
      isExternalTicketIdChecked: !this.state.isExternalTicketIdChecked,
    });
  }

  isShopCheckmarked() {
    this.setState({
      isShopChecked: !this.state.isShopChecked,
    });
  }

  isAppCheckmarked() {
    this.setState({
      isAppChecked: !this.state.isAppChecked,
    });
  }

  isStatusCheckmarked() {
    this.setState({
      isStatusChecked: !this.state.isStatusChecked,
    });
  }

  isMethodsCheckmarked() {
    this.setState({
      isMethodsChecked: !this.state.isMethodsChecked,
    });
  }

  isUserCheckmarked() {
    this.setState({
      isUserChecked: !this.state.isUserChecked,
    });
  }
  
  async setPaginatedTicketsList(){
    let tickets = this.state.tickets_list
    const pageSize = 10;
    let newTickets = _(tickets).slice(0).take(pageSize).value()

    await this.setState({
        paginatedTickets: newTickets
      })
  }

  handlePaginationClick = (page) => {
    const pageSize = 10;
    let tickets = this.state.tickets_list;

    this.setState({
      currentPage: page
    });

    const startIndex = (page - 1) * pageSize;
    const paginatedTickets = _(tickets).slice(startIndex).take(pageSize).value();

    this.setState({
      paginatedTickets: paginatedTickets
    })
  }

  onHandleChange = (event) => {
    this.setState(({ externalTicketId }) => ({
      externalTicketId: event
    }), () => {
      this.setState(({ externalTicketId }) => ({
        externalTicketId: event
      }))
    })
    console.log("External id: ", this.state.externalTicketId);
  }

  onHandleUrlChange = (event) => {
    this.setState(({ shopUrl }) => ({
      shopUrl: event
    }), () => {
      this.setState(({ shopUrl }) => ({
        shopUrl: event
      }))
    })
    console.log("Shop URL: ", this.state.shopUrl);
  }

  searchTicket(){
    let allTickets = this.state.tickets_list
    let ext = this.state.externalTicketId
    let newTicket = []

    console.log(this.state.tickets_list);

    for(let i = 0; i < allTickets.length; i++){
      if(allTickets[i]["external_ticket_id"] == ext){
        newTicket.push(allTickets[i]);

        this.setState(({ searchResult }) => ({
          searchResult: newTicket,
          showUrlSearchResultColumn: false,
          showSearchResultColumn: true
        }), () => {
          this.setState(({ searchResult }) => ({
            searchResult: newTicket,
            showUrlSearchResultColumn: false,
            showSearchResultColumn: true
          }))
        })
        console.log(this.state.searchResult);
        console.log(this.state.showSearchResultColumn);
      } else {
        console.log("Ext not found");
      }
    }
  }

  searchTicketUsingUrl(){
    let allTickets = this.state.tickets_list
    let url = this.state.shopUrl
    let newTicket = []

    console.log(this.state.tickets_list);

    for(let i = 0; i < allTickets.length; i++){
      if(allTickets[i]["shop_url"] == url){
        newTicket.push(allTickets[i]);

        this.setState(({ urlSearchResult }) => ({
          urlSearchResult: newTicket,
          showSearchResultColumn: false,
          showUrlSearchResultColumn: true
        }), () => {
          this.setState(({ urlSearchResult }) => ({
            urlSearchResult: newTicket,
            showSearchResultColumn: false,
            showUrlSearchResultColumn: true
          }))
        })
        console.log(this.state.urlSearchResult);
        console.log(this.state.showUrlSearchResultColumn);
      } else {
        console.log("URL not found");
      }
    }
  }

  render() {
    const sortTypes = {
      up: {
        class: "sort-up",
        fn: (a, b) => new Date(a.created_at) - new Date(b.created_at),
      },
      down: {
        class: "sort-down",
        fn: (a, b) => new Date(b.created_at) - new Date(a.created_at),
      },
      default: {
        class: "sort",
        fn: (a, b) => a,
      },
    };
    const {
      isDateVisible,
      isUserVisible,
      isStatusVisible,
      isAppVisible,
      isIdVisible,
      isShopVisible,
      isExternalTicketIdVisible,
      isMethodsVisible,
      currentSort,
      tickets_list,
      paginatedTickets,
      currentPage,
      showSearchResultColumn,
      showUrlSearchResultColumn
    } = this.state;

    const pageSize = 10;
    let pageCount = tickets_list ? Math.ceil(tickets_list.length/pageSize) : 0;
   
    /* A blank page issue happened when I add this line of code,
     if (pageCount === 1) return null; */

    let pages = _.range(1, pageCount + 1)  


    if(showSearchResultColumn === true){
            
                  <Row style={{
                    marginLeft: "40px",
                    marginRight: "40px",
                    marginTop: "0px",
                    color: "white",
                  }}>
                    <Table
                      striped
                      bordered
                      hover
                      style={{ color: "white" }}
                      ref="table"
                    >
                      
                      <tbody>
                        {[...this.state.searchResult]
                          .sort(sortTypes[currentSort].fn)
                          .map((sResult) => (
                            <tr
                              key={Math.random()}
                              onClick={() =>
                                history.push(`/ticket?id=${sResult.id}`)
                              }
                            >
                              <td
                                key={Math.random()}
                                style={
                                  isDateVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.created_at.slice(0, 10)}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isExternalTicketIdVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.external_ticket_id}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isShopVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.shop_url}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isAppVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.app.name}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isStatusVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.status}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isMethodsVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.command}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isUserVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.user.first_name}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </Row>
               
    }

    if(showUrlSearchResultColumn === true){
     
          <Row style={{
            marginLeft: "40px",
            marginRight: "40px",
            marginTop: "0px",
            color: "white",
          }}>
            <Table
              striped
              bordered
              hover
              style={{ color: "white" }}
              ref="table"
            >
              
              <tbody>
                {[...this.state.urlSearchResult]
                  .sort(sortTypes[currentSort].fn)
                  .map((sResult) => (
                    <tr
                      key={Math.random()}
                      onClick={() =>
                        history.push(`/ticket?id=${sResult.id}`)
                      }
                    >
                      <td
                        key={Math.random()}
                        style={
                          isDateVisible == false
                            ? showDateColumn
                            : hideDateColumn
                        }
                      >
                        {sResult.created_at.slice(0, 10)}
                      </td>
                      <td
                        key={Math.random()}
                        style={
                          isExternalTicketIdVisible == false
                            ? showDateColumn
                            : hideDateColumn
                        }
                      >
                        {sResult.external_ticket_id}
                      </td>
                      <td
                        key={Math.random()}
                        style={
                          isShopVisible == false
                            ? showDateColumn
                            : hideDateColumn
                        }
                      >
                        {sResult.shop_url}
                      </td>
                      <td
                        key={Math.random()}
                        style={
                          isAppVisible == false
                            ? hideDateColumn
                            : showDateColumn
                        }
                      >
                        {sResult.app.name}
                      </td>
                      <td
                        key={Math.random()}
                        style={
                          isStatusVisible == false
                            ? hideDateColumn
                            : showDateColumn
                        }
                      >
                        {sResult.status}
                      </td>
                      <td
                        key={Math.random()}
                        style={
                          isMethodsVisible == false
                            ? hideDateColumn
                            : showDateColumn
                        }
                      >
                        {sResult.command}
                      </td>
                      <td
                        key={Math.random()}
                        style={
                          isUserVisible == false
                            ? hideDateColumn
                            : showDateColumn
                        }
                      >
                        {sResult.user.first_name}
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Row>
           
}

    const hideDateColumn = { display: "none" };
    const showDateColumn = { display: "" };
    const { history } = this.props;

    return (
      <Router>
        <div>
          <div>
            <Row style={{ width: "100%" }}>
              <Col sm={1}>
                <Navbar
                  expand="sm"
                  style={{ backgroundColor: "#13547a", height: "950px", width: "70px" }}
                  className="flex-column"
                >

                  <Nav className="flex-column">
                     <Nav.Link style={{ color: "white" }}
                        onClick={() => history.push(`/tickets`)}>
                      <span style={{ fontSize: "25px" }}>
                      <i className="fas fa-th-list"></i>
                      </span>
                      </Nav.Link>

                      <Nav.Link style={{ color: "white" }}
                      onClick={() => history.push(`/settings`)}>
                      <span style={{ fontSize: "25px" }}>
                       <i className="fas fa-cogs"></i>
                      </span>
                    </Nav.Link>

                  </Nav>
                </Navbar>
              </Col>

              <Col sm={11} style={{ width: "100%" }}>
                <Navbar
                  style={{
                    backgroundColor: "#13547a",
                    height: "110px",
                    width: "100%",
                    marginBottom: "20px"
                  }}
                >
                  <Row style={{ width: "100%" }} onMouseEnter={this.setPaginatedTicketsList}>
                    <Col sm={4}>
                      <Form inline style={{ width: "400px" }}>
                        <FormControl
                          type="text"
                          name="externalTicketId"
                          style={{ width: "230px" }}
                          value={this.state.externalTicketId}
                          placeholder="Search by External Ticket ID"
                          className="mr-sm-2"
                          onChange={(event) => this.onHandleChange(event.target.value)}
                        />
                        <Button
                          style={{
                            backgroundColor: "#13547a",
                            borderColor: "white",
                          }}
                          onClick={this.searchTicket}
                        >
                          Search
                        </Button>
                      </Form>
                    </Col>

                    <Col sm={4}>
                      <Form inline style={{ width: "400px" }}>
                        <FormControl
                          type="text"
                          name="shopUrl"
                          style={{ width: "230px" }}
                          value={this.state.shopUrl}
                          placeholder="Search by Shop URL"
                          className="mr-sm-2"
                          onChange={(event) => this.onHandleUrlChange(event.target.value)}
                        />
                        <Button
                          style={{
                            backgroundColor: "#13547a",
                            borderColor: "white",
                          }}
                          onClick={this.searchTicketUsingUrl}
                        >
                          Search
                        </Button>
                      </Form>
                    </Col>

                    <Col sm={2}>
                      <Dropdown>
                        <Dropdown.Toggle
                          style={{
                            backgroundColor: "#13547a",
                            borderColor: "white",
                          }}
                          id="dropdown-basic"
                        >
                          Filter tickets
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                          <Dropdown.Item>Date</Dropdown.Item>
                          <Dropdown.Item>External Ticket ID</Dropdown.Item>
                          <Dropdown.Item>Shop</Dropdown.Item>
                          <Dropdown.Item>App</Dropdown.Item>
                          <Dropdown.Item>Status</Dropdown.Item>
                          <Dropdown.Item>Command</Dropdown.Item>
                          <Dropdown.Item>User</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                    </Col>

                    <Col sm={2}>
                      <Dropdown>
                        <Dropdown.Toggle
                          style={{
                            backgroundColor: "#13547a",
                            borderColor: "white",
                          }}
                          id="dropdown-basic"
                        >
                          Add Columns
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                          <Dropdown.Item
                            onClick={() => {
                              this.makeDateVisible();
                              this.isDateCheckmarked();
                            }}
                            className={
                              this.state.isDateChecked == false
                                ? "dropdown-item-checked"
                                : "nothing"
                            }
                          >
                            Date
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => {
                              this.makeExternalTicketIdVisible();
                              this.isExternalTicketIdCheckmarked();
                            }}
                            className={
                              this.state.isExternalTicketIdChecked == false
                                ? "dropdown-item-checked"
                                : "nothing"
                            }
                          >
                            External Ticket ID
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => {
                              this.makeShopVisible();
                              this.isShopCheckmarked();
                            }}
                            className={
                              this.state.isShopChecked == false
                                ? "dropdown-item-checked"
                                : "nothing"
                            }
                          >
                            Shop
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => {
                              this.makeAppVisible();
                              this.isAppCheckmarked();
                            }}
                            className={
                              this.state.isAppChecked == true
                                ? "dropdown-item-checked"
                                : "nothing"
                            }
                          >
                            App
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => {
                              this.makeStatusVisible();
                              this.isStatusCheckmarked();
                            }}
                            className={
                              this.state.isStatusChecked == true
                                ? "dropdown-item-checked"
                                : "nothing"
                            }
                          >
                            Status
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => {
                              this.makeMethodsVisible();
                              this.isMethodsCheckmarked();
                            }}
                            className={
                              this.state.isMethodsChecked == true
                                ? "dropdown-item-checked"
                                : "nothing"
                            }
                          >
                            Command
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => {
                              this.makeUserVisible();
                              this.isUserCheckmarked();
                            }}
                            className={
                              this.state.isUserChecked == true
                                ? "dropdown-item-checked"
                                : "nothing"
                            }
                          >
                            User
                          </Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                    </Col>
                  </Row>
                </Navbar>
            
            {/* The below div is used to return the search result for an external ticket id */}
          
                  <Row style={{
                    marginLeft: "40px",
                    marginRight: "40px",
                    marginTop: "0px",
                    color: "white",
                  }}>
                    <Table
                      striped
                      bordered
                      hover
                      style={{ color: "white" }}
                      ref="table"
                    >
                      
                      <tbody>
                        {[...this.state.searchResult]
                          .sort(sortTypes[currentSort].fn)
                          .map((sResult) => (
                            <tr
                              key={Math.random()}
                              onClick={() =>
                                history.push(`/ticket?id=${sResult.id}`)
                              }
                            >
                              <td
                                key={Math.random()}
                                style={
                                  isDateVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.created_at.slice(0, 10)}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isExternalTicketIdVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.external_ticket_id}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isShopVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.shop_url}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isAppVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.app.name}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isStatusVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.status}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isMethodsVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.command}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isUserVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.user.first_name}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </Row>
               
        {/* The above div is used to return the search result for an external ticket id */}

        {/* The below div is used to return the search result for shop URL */}
       
                  <Row style={{
                    marginLeft: "40px",
                    marginRight: "40px",
                    marginTop: "0px",
                    color: "white",
                  }}>
                    <Table
                      striped
                      bordered
                      hover
                      style={{ color: "white" }}
                      ref="table"
                    >
                      
                      <tbody>
                        {[...this.state.urlSearchResult]
                          .sort(sortTypes[currentSort].fn)
                          .map((sResult) => (
                            <tr
                              key={Math.random()}
                              onClick={() =>
                                history.push(`/ticket?id=${sResult.id}`)
                              }
                            >
                              <td
                                key={Math.random()}
                                style={
                                  isDateVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.created_at.slice(0, 10)}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isExternalTicketIdVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.external_ticket_id}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isShopVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {sResult.shop_url}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isAppVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.app.name}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isStatusVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.status}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isMethodsVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.command}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isUserVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {sResult.user.first_name}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </Row>
              
        {/* The above div is used to return the search result for shop URL */}


                <div
                  style={{
                    marginLeft: "40px",
                    marginRight: "40px",
                    marginTop: "0px",
                    color: "white",
                  }}
                  onMouseEnter={this.setPaginatedTicketsList}
                >
                  <h6 style={{ marginTop: "20px" }}>
                    Here are all the open tickets:
                  </h6>
                  <Row className="mt-4">
                    <Table
                      striped
                      bordered
                      hover
                      style={{ color: "white" }}
                      ref="table"
                    >
                      <thead>
                        <tr>
                          <th
                            style={
                              isDateVisible == false
                                ? showDateColumn
                                : hideDateColumn
                            }
                          >
                            Date
                            <button onClick={this.onSortChange}>
                              <i
                                className={`fas fa-${sortTypes[currentSort].class}`}
                              />
                            </button>
                          </th>
                          <th
                            style={
                              isExternalTicketIdVisible == false
                                ? showDateColumn
                                : hideDateColumn
                            }
                          >
                            External Ticket ID
                          </th>
                          <th
                            style={
                              isShopVisible == false
                                ? showDateColumn
                                : hideDateColumn
                            }
                          >
                            Shop
                          </th>
                          <th
                            style={
                              isAppVisible == false
                                ? hideDateColumn
                                : showDateColumn
                            }
                          >
                            App
                          </th>
                          <th
                            style={
                              isStatusVisible == false
                                ? hideDateColumn
                                : showDateColumn
                            }
                          >
                            Status
                          </th>
                          <th
                            style={
                              isMethodsVisible == false
                                ? hideDateColumn
                                : showDateColumn
                            }
                          >
                            Command
                          </th>
                          <th
                            style={
                              isUserVisible == false
                                ? hideDateColumn
                                : showDateColumn
                            }
                          >
                            User
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {[...paginatedTickets]
                          .sort(sortTypes[currentSort].fn)
                          .map((ticket_list) => (
                            <tr
                              key={Math.random()}
                              onClick={() =>
                                history.push(`/ticket?id=${ticket_list.id}`)
                              }
                            >
                              <td
                                key={Math.random()}
                                style={
                                  isDateVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {ticket_list.created_at.slice(0, 10)}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isExternalTicketIdVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {ticket_list.external_ticket_id}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isShopVisible == false
                                    ? showDateColumn
                                    : hideDateColumn
                                }
                              >
                                {ticket_list.shop_url}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isAppVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {ticket_list.app.name}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isStatusVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {ticket_list.status}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isMethodsVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {ticket_list.command}
                              </td>
                              <td
                                key={Math.random()}
                                style={
                                  isUserVisible == false
                                    ? hideDateColumn
                                    : showDateColumn
                                }
                              >
                                {ticket_list.user.first_name}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </Row>

                  <nav className="d-flex justify-content-center">
                    <ul className="pagination">
                      {
                        pages.map((page) => (
                          <li className={
                            page === currentPage ? "page-item active" : "page item"
                          } key={Math.random()}>
                            <p className="page-link"
                            onClick={() => this.handlePaginationClick(page)}>{page}</p>
                          </li>
                        ))
                      }
                    </ul>
                  </nav>
        
        
                  <Row style={{ marginBottom: "30px" }}>
                    <Col sm={8}></Col>
                    <Col sm={2}>
                      <Button
                        style={{
                          backgroundColor: "#13547a",
                          borderColor: "#13547a",
                        }}
                      onClick={() => history.push(`/new_ticket`)}
                      >
                        New Ticket
                      </Button>
                    </Col>
                    <Col sm={2}>
                      <Button
                        style={{
                          backgroundColor: "#13547a",
                          borderColor: "#13547a",
                        }}
                      >
                        Refresh
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>

        </div>
      </Router>
    );
  }
}

export default withRouter(Tickets);

