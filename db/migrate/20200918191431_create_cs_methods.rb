class CreateCsMethods < ActiveRecord::Migration[6.0]
  def change
    create_table :cs_methods do |t|
      t.integer :external_ticket_id
      t.integer :user_id
      t.text :arguments_hash
      t.text :results_hash
      t.integer :cs_method_template_id
      t.string :status

      t.timestamps
    end
  end
end
