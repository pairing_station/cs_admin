class CreateCommands < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.integer :external_ticket_id
      t.string :shop_url
      t.integer :app_id
      t.integer :user_id
      t.string :status

      t.timestamps
    end
  end
end
