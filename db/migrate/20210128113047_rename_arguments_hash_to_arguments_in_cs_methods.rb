class RenameArgumentsHashToArgumentsInCsMethods < ActiveRecord::Migration[6.0]
  def change
    rename_column :cs_methods, :arguments_hash, :arguments
  end
end
