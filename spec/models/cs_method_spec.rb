require 'rails_helper'

RSpec.describe CsMethod, :type => :model do
  subject { 
         described_class.new(
                ticket: FactoryBot.create(:ticket),
                user: FactoryBot.create(:user),
                arguments: {"mel": "bel", "zap": "map"},
                results: {"results": "results"},
                cs_method_template: FactoryBot.create(:cs_method_template),
                status: "Active",
                external_ticket_id: 19
         )  
  }

  describe "Validations" do

    it "is valid with valid attributes" do
        expect(subject).to be_valid
    end 
    
    it "is not valid without ticket_id" do
        subject.ticket_id = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without user_id" do
        subject.user_id = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without arguments" do
        subject.arguments = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without a cs_method_template_id" do
      subject.cs_method_template_id = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without status" do
        subject.status = nil
        expect(subject).to_not be_valid
      end

  end

  describe "Associations" do

    it { should belong_to(:cs_method_template) }
    it { should belong_to(:user) }
    
  end

end