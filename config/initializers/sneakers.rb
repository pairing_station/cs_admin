require 'sneakers'
require 'sneakers/handlers/maxretry'

Sneakers.configure  :heartbeat => 30,
                    :amqp => ENV['AMQP_URL'] || 'amqp://guest:guest@localhost:5672',
                    #:vhost => '/', # do we need this line?
                    :exchange => 'sneakers',
                    :exchange_type => :direct,
                    handler: Sneakers::Handlers::Maxretry,
                    workers: (ENV['SNEAKERS_WORKER_QUANTITY'] || 1).to_i

Sneakers.logger.level = Logger::INFO

WORKER_OPTIONS = {
    retry_timeout:      60 * 1000,  # 1 minute
    ack:                true,
    threads:            (ENV['SNEAKERS_DEFAULT_THREAD_QUANTITY'].present? ? ENV['SNEAKERS_DEFAULT_THREAD_QUANTITY'].to_i : 10),
    prefetch:           (ENV['SNEAKERS_DEFAULT_PREFETCH_QUANTITY'].present? ? ENV['SNEAKERS_DEFAULT_PREFETCH_QUANTITY'].to_i : 10),
}
