class User < ApplicationRecord
  has_many :tickets
  has_many :cs_methods

  validates_presence_of :email
end
