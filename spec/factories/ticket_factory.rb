FactoryBot.define do
  factory :ticket do
    user {FactoryBot.create(:user)}
    external_ticket_id {19}
    shop_url {"shop.com"}
    app {FactoryBot.create(:app)}
    status {"Active"}
  end
end