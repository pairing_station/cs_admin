class TicketsController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def tickets
    render layout: 'application'
  end

  def ticket
    render layout: 'application'
  end

  def ticket_detail
    ticket = Ticket.find(params[:id])
    render json: { ticket: ticket }, status: 200 
  end

  def get_new_ticket
    new_ticket = Ticket.last
    render json: { new_ticket: new_ticket }, status: 200
  end

  def apps_list
    apps = App.all
    render json: { list: apps }, status: 200
  end

  def csmethodtemplate_list
    csmethodtemplates = CsMethodTemplate.all
    render json: { list: csmethodtemplates }, status: 200
  end

  def tickets_list
    tickets = Ticket.all
    tickets_list = tickets.map do |ticket|
      {
        :id => ticket.id,
        :external_ticket_id => ticket.external_ticket_id,
        :status => ticket.status,
        :shop_url => ticket.shop_url,
        :created_at => ticket.created_at,
        :user => get_user(ticket.user_id),
        :app => get_app(ticket.app_id),
        :command => ticket.external_ticket_id
      }
    end
    render json: { list: tickets_list }, status: 200
  end

  def users_list
    users = User.all
    render json: { list: users }, status: 200
  end

  private

  def get_user(id)
    user = User.find id
    result = { :id => user.id, :first_name => user.first_name }
  end

  def get_app(id)
    app = App.find id
    result = { :id => app.id, :name => app.name }
  end

end
