class AddHelpToCsMethodTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :cs_method_templates, :help, :string
  end
end
