require 'rails_helper'

RSpec.describe NewTicketController, type: :controller do

  before do
    user = FactoryBot.create(:user)
    sign_in user
  end

  describe "GET #fill_new_ticket" do
    it "returns http success" do
      get :fill_new_ticket
      expect(response).to have_http_status(:success)
    end
  end

  # describe "POST new_ticket#create_ticket" do
  # it "should create a new ticket" do
  #  post :create_ticket, :params => { :external_ticket_id, :shop_url, :app_id, :user_id, :status }
  #  expect(response).to have_http_status(:created)
  # end
  #end

end
