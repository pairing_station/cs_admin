import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Link, BrowserRouter as Router } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import "../../assets/stylesheets/signup.css";

class Settings extends Component {
  render() {
    const { history } = this.props;
    return (
      <Router>
        <div className="main" style={{ backgroundColor: "#13547a" }}>
          <Form className="form">
            <div className="title">
              <h5>Refresh Commands</h5>
            </div>

            <Row>
              <Col>
              <h5>SyncLogic</h5>
              <Button
                type="submit"
                style={{ backgroundColor: "white", color: "black" }}
              >
                Refresh
              </Button>
              </Col>

              <Col>
              <h5>OrderLogic</h5>
              <Button
                type="submit"
                style={{ backgroundColor: "white", color: "black" }}
              >
                Refresh
              </Button>
              </Col>

              <Col>
              <h5>EmailAlchemy</h5>
              <Button
                type="submit"
                style={{ backgroundColor: "white", color: "black" }}
              >
                Refresh
              </Button>
              </Col>
            </Row>
 
              

            <p className="title" style={{ cursor: "pointer" }}>
                Go{" "}
                <a style={{ color: "white" }} onClick={() => history.push(`/tickets`)}>
                  Back
                </a>{" "}
            </p>

          </Form>
        </div>
      </Router>
    );
  }
}

export default withRouter(Settings);
