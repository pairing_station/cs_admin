import React, { Component } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import "../../assets/stylesheets/application.css";
import {
  getAppsList,
  getNewTicket
} from "./service/allServices";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { BrowserRouter as Router } from "react-router-dom";
import Alert from "react-bootstrap/Alert";
import { withRouter } from "react-router-dom";
import axios from 'axios';

class NewTicket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editedDate: null,
      user_name: "",
      external_ticket_id: null,
      shop_url: "",
      ticket_app: "",
      ticket_status: "",
      user_id: 20,
      app_id: null,
      showModal: false,
      new_ticket: [],
    };
    this.makeStatusClosed = this.makeStatusClosed.bind(this);
    this.makeStatusActive = this.makeStatusActive.bind(this);
    this.makeStatusPending = this.makeStatusPending.bind(this);
    this.makeStatusBlocked = this.makeStatusBlocked.bind(this);
    this.makeAppEmailAlchemy = this.makeAppEmailAlchemy.bind(this);
    this.makeAppOrderLogic = this.makeAppOrderLogic.bind(this);
    this.makeAppSyncLogic = this.makeAppSyncLogic.bind(this);
    this.onHandleChange = this.onHandleChange.bind(this);
    this.splitDate = this.splitDate.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
    this.onNewTicketSubmit = this.onNewTicketSubmit.bind(this);
    this.refreshPage = this.refreshPage.bind(this);
  }

  componentDidMount() {
    this.fetchAppsList();
    this.fetchNewTicket();
  }

  async fetchAppsList() {
    const { data } = await getAppsList();
    const { list, status } = data;
    this.setState({ apps: list });
  }

  async fetchNewTicket(){
    const { data } = await getNewTicket();
    const { new_ticket, status } = data;
    this.setState({ new_ticket: new_ticket });
  }

  makeStatusClosed() {
    this.setState({
      ticket_status: "Closed"
    })
  }

  makeStatusActive() {
    this.setState({
      ticket_status: "Active"
    })
  }

  makeStatusPending() {
    this.setState({
      ticket_status: "Pending"
    })
  }

  makeStatusBlocked() {
    this.setState({
      ticket_status: "Blocked"
    })
  }

  makeAppEmailAlchemy(){
    this.setState({
      ticket_app: "EmailAlchemy",
      app_id: 3
    })
  }

  makeAppOrderLogic(){
    this.setState({
      ticket_app: "OrderLogic",
      app_id: 2
    })
  }

  makeAppSyncLogic(){
    this.setState({
      ticket_app: "SyncLogic",
      app_id: 1
    })
  }
  
  onHandleChange = (evt) => {
    const value = evt.target.value;
    this.setState({
    ...this.state,
    [evt.target.name]: value
  });
    console.log("extID: ", this.state.external_ticket_id);
    console.log("user name: ", this.state.user_name);
    console.log("shop_url: ", this.state.shop_url);
  }

  onNewTicketSubmit = () => {
    if(this.state.external_ticket_id != null){
      axios({
        method: 'post',
        url: 'http://localhost/all_tickets',
        data: {
          external_ticket_id: this.state.external_ticket_id,
          shop_url: this.state.shop_url,
          status: this.state.ticket_status,
          user_id: this.state.user_id,
          app_id: this.state.app_id  
      },
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        console.log(response)
      })
      .catch((error) => {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
    })}  
    console.log("user id: ", this.state.user_id);
    console.log("status: ", this.state.ticket_status);
    console.log("shop url: ", this.state.shop_url);
    console.log("external ticket id: ", this.state.external_ticket_id);
    console.log("app_id: ", this.state.app_id);
    }

  refreshPage = () => {
      window.location.reload();
   }

  splitDate(){
    let date = new Date().toISOString().slice(0, 10);
    this.setState({
      editedDate: date
    })
  }

  closeModal(){
    this.setState({
      showModal: false
    })
  }

  openModal(){
    this.setState({
      showModal: true
    })
  }

  render() {
    const { history } = this.props;
    
    const {
      ticket_app,
      ticket_status
    } = this.state;
 
   console.log("app ", ticket_app);
   console.log("status ", ticket_status);

    return (
      <Router>
        <Alert dismissible onClose={() => history.push(`/tickets`)}>
          <div>
            <div>
              <Row style={{ width: "100%" }}>
                <Col sm={1}>
                  <Navbar
                    expand="sm"
                    style={{ backgroundColor: "#13547a", height: "800px", width: "70px" }}
                    className="flex-column"
                  >

                    <Nav className="flex-column">

                      <Nav.Link style={{ color: "white" }}
                        onClick={() => history.push(`/tickets`)}>
                      <span style={{ fontSize: "25px" }}>
                      <i className="fas fa-th-list"></i>
                      </span>
                      </Nav.Link>

                      <Nav.Link style={{ color: "white" }}
                      onClick={() => history.push(`/settings`)}>
                      <span style={{ fontSize: "25px" }}>
                       <i className="fas fa-cogs"></i>
                      </span>
                      </Nav.Link>

                    </Nav>
                  </Navbar>
                </Col>

                <Col sm={11} style={{ width: "100%", margin: "0" }}>
                  <Form
                    style={{
                      backgroundColor: "rgb(70,136,155)",
                      paddingTop: "35px",
                      paddingRight: "20px",
                      paddingLeft: "20px",
                      paddingBottom: "1px",
                      height: "400px"
                    }}

                    onMouseEnter={this.splitDate}
                  >
                    <Form.Group as={Row}>
                      <Form.Label column sm={1} style={{ color: "white" }}>
                        External Ticket Id
                      </Form.Label>
                      <Col sm={3}>
                        <Form.Control type="number" name="external_ticket_id" value={this.state.external_ticket_id} 
                        onChange={this.onHandleChange} />
                      </Col>
                      <Col sm={2}></Col>
                      <Form.Label column sm={1} style={{ color: "white" }}>
                        Date
                      </Form.Label>
                      <Col sm={4}>
                        <Form.Control
                          plaintext
                          style={{ color: "white" }}
                          readOnly
                          defaultValue={this.state.editedDate}
                        />
                      </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                      <Form.Label column sm={1} style={{ color: "white" }}>
                        Shop
                      </Form.Label>
                      <Col sm={3}>
                        <Form.Control
                          type="text"
                          name="shop_url"
                          style={{ color: "#495057", backgroundColor: "white" }}
                          value={this.state.shop_url}
                          onChange={this.onHandleChange}
                        />
                      </Col>
                      <Col sm={2}></Col>

                      <Col sm={1}>
                      <Form.Label style={{ color: "white" }}>
                        App
                      </Form.Label>
                      </Col>

                      <Col sm={2}>
                      <Dropdown>
                        <Dropdown.Toggle className="button-group" id="dropdown-basic">
                          {this.state.ticket_app || "Select App" } 
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                          <Dropdown.Item onClick={this.makeAppSyncLogic} value={1}>SyncLogic</Dropdown.Item>
                          <Dropdown.Item onClick={this.makeAppOrderLogic} value={2}>OrderLogic</Dropdown.Item>
                          <Dropdown.Item onClick={this.makeAppEmailAlchemy} value={3}>EmailAlchemy</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                      </Col>
                    </Form.Group>

                    <Form.Group as={Row} style={{marginTop: "35px"}}>
                      <Form.Label column sm={1} style={{ color: "white" }}>
                        User
                      </Form.Label>
                      <Col sm={3}>
                        <Form.Control
                          type="text"
                          name="user_name"
                          style={{ color: "#495057", backgroundColor: "white" }}
                          value={this.state.user_name}
                          onChange={this.onHandleChange}
                        />
                      </Col>
                      <Col sm={2}></Col>

                      <Col sm={1}>
                      <Form.Label style={{ color: "white" }}>
                        Status
                      </Form.Label>
                      </Col>

                      <Col sm={2}>
                      <Dropdown>
                        <Dropdown.Toggle className="button-group" id="dropdown-basic">
                          {this.state.ticket_status || "Select Status"}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                          <Dropdown.Item onClick={this.makeStatusActive}>Active</Dropdown.Item>
                          <Dropdown.Item onClick={this.makeStatusPending}>Pending</Dropdown.Item>
                          <Dropdown.Item onClick={this.makeStatusClosed}>Closed</Dropdown.Item>
                          <Dropdown.Item onClick={this.makeStatusBlocked}>Blocked</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                      </Col>
                      
                    
                    </Form.Group>

                    <Form.Group as={Row} style={{ marginTop: "35px" }}>
                      <Col sm={8}></Col>

                      <Col sm={2}>
                        <Button
                          style={{
                            backgroundColor: "#13547a",
                            borderColor: "#13547a",
                          }}
                          onClick={() => { this.onNewTicketSubmit(); /* this.refreshPage(); */ }}
                        >
                          Create Ticket
                        </Button>
                      </Col>

                      <Col sm={2}>
                        <Button style={{
                            backgroundColor: "#13547a",
                            borderColor: "#13547a",
                          }}
                        onClick={() => { history.push(`/ticket?id=${this.state.new_ticket.id}`); this.refreshPage(); }}
                        >Open Ticket</Button>
                      </Col>

                    </Form.Group>
                  </Form>
               
                </Col>
              </Row>
            </div>
          </div>
        </Alert>
      </Router>
    );
  }
}
export default withRouter(NewTicket);
