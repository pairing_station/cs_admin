require 'rails_helper'

RSpec.describe User, :type => :model do
  subject { 
         described_class.new(
                first_name: "melkam",
                last_name: "melkonne",
                password: "some_password", 
                email: "john@doe.com",
                status: "Active"
         )  
  }

  describe "Validations" do

    it "has a valid factory" do
      expect(FactoryBot.create :user).to be_valid
    end

    it "is valid with valid attributes" do
        expect(subject).to be_valid
    end

    it "is not valid without a password" do
      subject.password = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without an email" do
      subject.email = nil
      expect(subject).to_not be_valid
    end

  end
end