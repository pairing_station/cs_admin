class RenameArgumentsListToArgumentsInCsMethodTemplates < ActiveRecord::Migration[6.0]
  def change
    rename_column :cs_method_templates, :arguments_list, :arguments
  end
end
