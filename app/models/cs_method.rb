class CsMethod < ApplicationRecord
   belongs_to :cs_method_template 
   belongs_to :user
   belongs_to :ticket

   after_create :send_to_app

   # adding - belongs_to :ticket cauesed a seed 0 error on cs_methods
   
   serialize :arguments, Hash
   serialize :results, Hash

   validates_presence_of :ticket_id, :arguments, :user_id, :cs_method_template_id, :status

   def send_to_app
     RequestService.send_request app_name: self.cs_method_template.app.name.downcase, method_name: self.cs_method_template.name, method_args: self.arguments, method_id: self.id
   end
end
