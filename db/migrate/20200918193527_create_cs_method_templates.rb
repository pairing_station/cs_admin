class CreateCsMethodTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :cs_method_templates do |t|
      t.integer :app_id
      t.string :name
      t.text :arguments_list

      t.timestamps
    end
  end
end
