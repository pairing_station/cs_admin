class CsMethodsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create_cs_method
    # @user = User.find(params[:id])

    # unless @user == current_user
    #   redirect_to :back, :alert => "Access denied."
    # end

    cs_method = CsMethod.create(cs_params)
      if cs_method.save
        render json: cs_method, status: :created
      else
        render json: cs_method.errors, status: :unprocessable_entity
      end
  end

  def cs_methods_list
    cs_methods = CsMethod.all
    render json: { list: cs_methods }, status: 200
  end

  private

  def cs_params
  params.permit(:ticket_id, :user_id, :cs_method_template_id, { :results => {} }, 
  { :arguments => {} }, :status, :external_ticket_id, :app_id)
  end
end
