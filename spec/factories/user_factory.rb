FactoryBot.define do
  factory :user do
    sequence(:email) {|n| "#{n}_user@gmail.com"}
  end
end