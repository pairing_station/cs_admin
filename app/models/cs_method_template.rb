class CsMethodTemplate < ApplicationRecord
    has_many :cs_methods
    belongs_to :app
    serialize :arguments, Hash

    validates_presence_of :app_id, :name, :arguments, :help
end
