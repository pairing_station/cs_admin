class RenameColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :cs_methods, :external_ticket_id, :ticket_id 
  end
end
