# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

App.destroy_all
User.destroy_all
CsMethodTemplate.destroy_all
CsMethod.destroy_all
Ticket.destroy_all

App.create([{name: "SyncLogic", id: 1}, {name: "OrderLogic", id: 2}, {name: "Email Alchemy", id: 3}])
p "Created #{App.count} App entries."

13.times do
   User.create(
       first_name: Faker::Name.first_name,
       last_name: Faker::Name.middle_name,
       email: Faker::Internet.email,
       status: Faker::Subscription.status
       )
end
p "Created #{User.count} User entries."

13.times do
    CsMethodTemplate.create(
        app_id: rand(1..3),
        name: Faker::Name.first_name,
        arguments: Hash[*Faker::Lorem.words(number: 4)],
        help: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4)      
    )
end
p "Created #{CsMethodTemplate.count} CsMethodTemplate entries."

13.times do
    Ticket.create(
        external_ticket_id: Faker::Number.number(digits: 4),
        shop_url: Faker::Internet.url,
        app_id: rand(1..3),
        user_id: rand(14..27),
        status: Faker::Subscription.status
    )
end
p "Created #{Ticket.count} Ticket entries."

45.times do
    CsMethod.create(
        ticket_id: rand(1..13),
        user_id: rand(14..27),
        cs_method_template_id: rand(14..27),
        arguments: Hash[*Faker::Lorem.words(number: 4)],
        results: Hash[*Faker::Lorem.words(number: 4)],
        status: Faker::Subscription.status,
        external_ticket_id: Faker::Number.number(digits: 4),
        app_id: rand(1..3)
    )
end
p "Created #{CsMethod.count} CsMethod entries."



