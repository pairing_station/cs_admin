import http from "./httpService";

export function getAppsList() {
  return http.get(`/apps`);
}

export function getTicketsList() {
  return http.get(`/tickets_api`);
}

export function getCsMethodsList() {
  return http.get(`/cs_methods`);
}

export function getTicketApi(id) {
  return http.get(`/ticket_detail/${id}`);
}

export function getNewTicket(){
  return http.get(`/created_ticket`);
}

export function getCsMethodTemplateList() {
  return http.get(`/csmethodtemplate_list`);
}

export function getUsersList(){
  return http.get(`/users`);
}

export function getAllTickets(){
  return http.get(`/all_tickets`);
}


