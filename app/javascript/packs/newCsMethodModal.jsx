import React, {Component} from 'react';
import {Modal, Button} from 'react-bootstrap';

class CreationModal extends Component {
    constructor(props){
        super(props);
    }

    render(){
    return (
        <Modal
      {...this.props}
      size="sm"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" style={{ fontSize: "20px" }}>
          A new cs method has been created!
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ fontSize: "17px" }}>
        Please close this modal and continue
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.props.onHide} style={{ backgroundColor: "#13547a", borderColor: "#13547a" }}>Close</Button>
      </Modal.Footer>
    </Modal>
    );
  }
}

export default CreationModal;