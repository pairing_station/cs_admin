class RenameResultsHashToResultsInCsMethods < ActiveRecord::Migration[6.0]
  def change
    rename_column :cs_methods, :results_hash, :results
  end
end
