require 'rails_helper'

RSpec.describe CsMethodsController, type: :controller do

  before do
    user = FactoryBot.create(:user)
  end

  describe "GET #cs_methods_list" do

    it "returns http success" do
      get :cs_methods_list
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST cs_methods#create_cs_method" do
   it "should create a new cs method" do
    post :create_cs_method, :params => { :ticket_id => 20, :user_id => FactoryBot.create(:user), :cs_method_template_id => 19, :results => { result: "result"},
         :arguments => { arg: "arg"}, :status => "Active", :external_ticket_id => 1234, :app_id => FactoryBot.create(:app) }

    expect(response).to have_http_status(:created)
    end
   end

end
