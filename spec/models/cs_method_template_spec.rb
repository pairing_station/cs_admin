require 'rails_helper'

RSpec.describe CsMethodTemplate, :type => :model do
  subject { 
         described_class.new(
                app: FactoryBot.create(:app),
                name: "melkam",
                arguments: {"mel": "bel", "banb": "zero"},
                help: "help"
         )  
  }

  describe "Validations" do

    it "has a valid factory" do
      expect(FactoryBot.create(:cs_method_template)).to be_valid
    end

    it "is valid with valid attributes" do
        expect(subject).to be_valid
    end 
    
    it "is not valid without app_id" do
        subject.app_id = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without name" do
        subject.name = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without arguments" do
        subject.arguments = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without help" do
      subject.help = nil
      expect(subject).to_not be_valid
     end

  end

  describe "Associations" do

    it { should belong_to(:app) }

  end

end