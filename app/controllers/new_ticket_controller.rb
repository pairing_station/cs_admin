class NewTicketController < ApplicationController
  skip_before_action :verify_authenticity_token

  def fill_new_ticket
    render layout: 'application'
  end

  def create_ticket
    ticket = Ticket.create(ticket_params)

    if ticket.save
      render json: ticket, status: :created
    else
      render json: ticket.errors, status: :unprocessable_entity
    end
  end

  def all_tickets
    tickets = Ticket.all
    render json: { list: tickets }, status: 200
  end

  def get_new_ticket
    new_ticket = Ticket.last
    render json: { new_ticket: new_ticket }, status: 200
  end

  private

  def ticket_params
    params.permit(:external_ticket_id, :shop_url, :app_id, :user_id, :status)
  end
end
